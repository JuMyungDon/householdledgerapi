package com.md.householdledgerapi.repository;

import com.md.householdledgerapi.entity.HouseholdLedger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseholdLedgerRepository extends JpaRepository<HouseholdLedger, Long> {
}
