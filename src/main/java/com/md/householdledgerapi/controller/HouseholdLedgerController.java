package com.md.householdledgerapi.controller;

import com.md.householdledgerapi.model.HouseholdLedgerItem;
import com.md.householdledgerapi.model.HouseholdLedgerRequest;
import com.md.householdledgerapi.service.HouseholdLedgerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/household-ledger")
public class HouseholdLedgerController {
    private final HouseholdLedgerService householdLedgerService;

    @PostMapping("/new")
    public String setHouseholdLedger(@RequestBody HouseholdLedgerRequest request) {
        householdLedgerService.setHouseholdLedger(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<HouseholdLedgerItem> getHouseholdLedgers() {
        return householdLedgerService.getHouseholdLedgers();
    }
}
