package com.md.householdledgerapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseholdLedgerRequest {
    private String moneyType;
    private Long money;
    private String category;
    private String etcMemo;
}