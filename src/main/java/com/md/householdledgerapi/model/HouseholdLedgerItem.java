package com.md.householdledgerapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HouseholdLedgerItem {
    private Long id;
    private LocalDate dateCreate;
    private String moneyType;
    private Long money;
    private String category;
}
