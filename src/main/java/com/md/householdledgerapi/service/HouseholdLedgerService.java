package com.md.householdledgerapi.service;

import com.md.householdledgerapi.entity.HouseholdLedger;
import com.md.householdledgerapi.model.HouseholdLedgerItem;
import com.md.householdledgerapi.model.HouseholdLedgerRequest;
import com.md.householdledgerapi.repository.HouseholdLedgerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HouseholdLedgerService {
    private final HouseholdLedgerRepository householdLedgerRepository;

    public void setHouseholdLedger(HouseholdLedgerRequest request) {
        HouseholdLedger addData = new HouseholdLedger();
        addData.setDateCreate(LocalDate.now());
        addData.setMoneyType(request.getMoneyType());
        addData.setMoney(request.getMoney());
        addData.setCategory(request.getCategory());
        addData.setEtcMemo(request.getEtcMemo());

        householdLedgerRepository.save(addData);
    }

    public List<HouseholdLedgerItem> getHouseholdLedgers() {
        List<HouseholdLedger> originList = householdLedgerRepository.findAll();

        List<HouseholdLedgerItem> result = new LinkedList<>();

        for (HouseholdLedger householdLedger : originList) {
            HouseholdLedgerItem addItem = new HouseholdLedgerItem();
            addItem.setId(householdLedger.getId());
            addItem.setDateCreate(householdLedger.getDateCreate());
            addItem.setMoneyType(householdLedger.getMoneyType());
            addItem.setMoney(householdLedger.getMoney());
            addItem.setCategory(householdLedger.getCategory());

            result.add(addItem);
        }

        return result;
    }
}
